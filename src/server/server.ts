//@ts-nocheck
const db = require('./db.json');
const fs = require('fs');
const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('./src/server/db.json');
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(jsonServer.bodyParser);

server.use('/author/:name', (req, res) => {
    const author: string = req.params.name;

    res.send(
        readPosts().filter(post => post.author === author)
    );
});

server.use(router);
server.listen(3000, () => {
    console.log('JSON Server is running');
});

function readPosts() {
    const dbRaw = fs.readFileSync('./src/server/db.json');
    const posts = JSON.parse(dbRaw).posts;

    return posts;
}
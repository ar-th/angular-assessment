import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { PostDetailsComponent } from './post-details.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ButtonModule,
        CardModule,
    ],
    exports: [],
    declarations: [PostDetailsComponent],
    providers: [],
})
export class PostDetailsModule { }

import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Post } from 'src/app/shared/types/post.type';
import { PostsActions, PostsSelectors } from '../state';
import { PostsState } from '../state/posts-state.type';
import { RouterHistoryService } from 'src/app/router-history/router-history.service';

@Component({
    selector: 'app-post-details',
    templateUrl: './post-details.component.html',
    styleUrls: ['./post-details.component.scss'],
    host: {
        class: 'post-details',
    }
})
export class PostDetailsComponent {
    public post$!: Observable<Post | undefined>;

    public error$!: Observable<string | null>;

    constructor(
        private store: Store<PostsState>,
        private route: ActivatedRoute,
        private historyService: RouterHistoryService,
    ) {
        const postId: number = parseInt(this.route.snapshot.params['id']);

        this.post$ = this.store.select(PostsSelectors.getViewedPost);
        this.error$ = this.store.select(PostsSelectors.getStateError);

        this.store.dispatch(PostsActions.setViewedPost({ postId }));
        this.store.dispatch(PostsActions.loadById({ postId }));
    }

    public back(): void {
        this.historyService.back();
    }
}

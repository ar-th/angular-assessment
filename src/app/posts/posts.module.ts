import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RouterHistoryService } from '../router-history/router-history.service';
import { AddPostModule } from './add-post/add-post.module';
import { AuthorPostsModule } from './author-posts/author-posts.module';
import { PostDetailsModule } from './post-details/post-details.module';
import { PostsTableModule } from './posts-table/posts-table.module';
import { PostsService } from './services/posts.service';
import { PostsEffects } from './state/posts-effects';
import { postsReducer } from './state/posts-reducer';

@NgModule({
    imports: [
        StoreModule.forFeature('posts', postsReducer),
        EffectsModule.forFeature([ PostsEffects ]),
        HttpClientModule,
        AddPostModule,
        AuthorPostsModule,
        PostDetailsModule,
        PostsTableModule,
    ],
    exports: [],
    declarations: [],
    providers: [
        PostsService,
    ],
})
export class PostsModule { }

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { AddPostComponent } from './add-post.component';
import { CardModule } from 'primeng/card';
import { FormFieldErrorModule } from '../../shared/components/form-field-error/form-field-error.module';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        FormFieldErrorModule,
        CardModule,
        ButtonModule,
        RouterModule
    ],
    exports: [],
    declarations: [AddPostComponent],
    providers: [],
})
export class AddPostModule { }

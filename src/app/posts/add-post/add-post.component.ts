import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, Subject, takeUntil } from 'rxjs';
import { RouterHistoryService } from 'src/app/router-history/router-history.service';
import { Post } from 'src/app/shared/types/post.type';
import { PostsActions, PostsSelectors } from '../state';
import { PostsState } from '../state/posts-state.type';

@Component({
    selector: 'app-add-post',
    templateUrl: './add-post.component.html',
    styleUrls: ['./add-post.component.scss'],
    host: {
        class: "add-post",
    },
})
export class AddPostComponent implements OnDestroy {
    public addPostFormGroup: FormGroup;

    public error$: Observable<string | null>;

    private destroyed$: Subject<void> = new Subject();

    constructor(
        private store: Store<PostsState>,
        private actions$: Actions,
        private router: Router,
        private historyService: RouterHistoryService,
        private formBuilder: FormBuilder,
    ) {
        this.error$ = this.store.select(PostsSelectors.getStateError);

        this.addPostFormGroup = formBuilder.group({
            id: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/)]],
            title: ['', [Validators.required, Validators.minLength(7)]],
            author: ['', Validators.required],
        });

        this.actions$.pipe(
            ofType(PostsActions.createPostSuccess),
            takeUntil(this.destroyed$),
        ).subscribe(() => {
            router.navigate(['/table']);
        });
    }

    public ngOnDestroy(): void {
        this.destroyed$.next();
    }

    public onSubmit(): void {
        const newPost: Post = this.addPostFormGroup.value;
        
        this.store.dispatch(PostsActions.createPost({ post: newPost }));
    }

    public back(): void {
        this.historyService.back();
    }
}

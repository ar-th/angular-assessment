import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Location } from '@angular/common'
import { Observable } from 'rxjs';
import { PostsState } from '../state/posts-state.type';
import { Author } from 'src/app/shared/types/author.type';
import { PostsActions, PostsSelectors } from '../state';
import { ActivatedRoute } from '@angular/router';
import { RouterHistoryService } from 'src/app/router-history/router-history.service';

@Component({
    selector: 'app-author-posts',
    templateUrl: './author-posts.component.html',
    styleUrls: ['./author-posts.component.scss'],
    host: {
        class: 'author-posts'
    },
})
export class AuthorComponent {
    public authorName: string;

    public authorPosts$: Observable<Author | undefined>;

    public error$: Observable<string | null>;

    constructor(
        private store: Store<PostsState>,
        private route: ActivatedRoute,
        private historyService: RouterHistoryService,
    ) {
        this.authorName = route.snapshot.params['name'];

        this.authorPosts$ = this.store.select(PostsSelectors.getViewedAuthorPosts);
        this.error$ = this.store.select(PostsSelectors.getStateError);

        this.store.dispatch(PostsActions.setViewedAuthor({ authorName: this.authorName }));
        this.store.dispatch(PostsActions.loadAuthor({ authorName: this.authorName }));
    }

    public back(): void {
        this.historyService.back();
    }
}

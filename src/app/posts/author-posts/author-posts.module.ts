import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { AuthorComponent as AuthorPostsComponent } from './author-posts.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        CardModule,
        ButtonModule,
    ],
    exports: [],
    declarations: [AuthorPostsComponent],
    providers: [],
})
export class AuthorPostsModule { }

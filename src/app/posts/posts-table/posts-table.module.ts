import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';

import { TableModule } from 'primeng/table';

import { PostsTableComponent } from './posts-table.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TableModule,
        ButtonModule,
        CardModule,
    ],
    exports: [],
    declarations: [PostsTableComponent],
    providers: [],
})
export class PostsTableModule { }

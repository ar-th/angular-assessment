import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Post } from 'src/app/shared/types/post.type';
import { PostsState } from '../state/posts-state.type';
import * as PostsActions from "../state/posts-actions";
import * as PostsSelectors from "../state/posts-selectors";

type PostTableColumnName = keyof Post | 'actions';

@Component({
    selector: 'app-posts-table',
    templateUrl: './posts-table.component.html',
    styleUrls: ['./posts-table.component.scss'],
    host: {
        class: 'posts-table',
    }
})
export class PostsTableComponent {
    public posts$!: Observable<Post[]>;

    public displayedColumns: PostTableColumnName[] = ['id', 'title', 'author', 'actions'];

    constructor(
        private store: Store<PostsState>,
    ) {
        this.store.dispatch(PostsActions.loadAll());
        
        this.posts$ = this.store.select(PostsSelectors.getAllPosts);
    }

    public deletePost(postId: number): void {
        this.store.dispatch(PostsActions.deletePost({ postId }));
    }
}

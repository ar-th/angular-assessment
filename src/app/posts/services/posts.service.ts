import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from 'src/app/shared/types/post.type';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class PostsService {
    private serverUrl: string = 'http://localhost:3000';

    constructor(
        private http: HttpClient,
    ) { }

    public getAll(): Observable<Post[]> {
        const url: string = `${this.serverUrl}/posts`

        return this.http.get<Post[]>(url);
    }

    public getById(id: number): Observable<Post> {
        const url: string = `${this.serverUrl}/posts/${id}`
        
        return this.http.get<Post>(url);
    }

    public getByAuthor(author: string): Observable<Post[]> {
        const url: string = `${this.serverUrl}/author/${author}`; // TODO: encode url

        return this.http.get<Post[]>(url);
    }

    public createPost(post: Post): Observable<Post> {
        const url: string = `${this.serverUrl}/posts`;

        const newPost: Post = {
            ...post,
            id: parseInt(post.id as any),
        };

        return this.http.post<Post>(url, newPost);
    }

    public deletePost(postId: number): Observable<any> {
        const url: string = `${this.serverUrl}/posts/${postId}`;

        return this.http.delete(url);
    }
}
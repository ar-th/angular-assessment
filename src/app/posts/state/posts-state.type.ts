import { Author } from "src/app/shared/types/author.type";
import { Post } from "src/app/shared/types/post.type";


export interface PostsState {
    posts: Post[];
    authors: Author[];
    viewedAuthor: string | null;
    viewedPost: number | null;
    error: string | null;
}
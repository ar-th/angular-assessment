import * as Selectors from './posts-selectors';
import * as Actions from './posts-actions';

export const PostsSelectors = Selectors;
export const PostsActions = Actions;

import { createAction, props } from "@ngrx/store";
import { Post } from "../../shared/types/post.type";

export const loadAll = createAction('[Posts] Load');

export const loadAllSuccess = createAction(
    '[Posts] Load Success',
    props<{ posts: Post[] }>(),
);

export const loadAllFailure = createAction(
    '[Posts] Load Failure',
    props<{ error: string }>(),
);

export const loadById = createAction(
    '[Posts] Load By Id',
    props<{ postId: number }>(),
);

export const loadByIdSuccess = createAction(
    '[Posts] Load By Id Success',
    props<{ post: Post }>(),
);

export const loadByIdFailure = createAction(
    '[Posts] Load By Id Failure',
    props<{ error: string }>(),
);

export const loadAuthor = createAction(
    '[Author] Load Posts',
    props<{ authorName: string }>(),
);

export const setViewedPost = createAction(
    '[Post] Set Viewed Post',
    props<{ postId: number }>(),
)

export const clearViewedPost = createAction(
    '[Post] Clear Viewed Post',
);

export const loadAuthorSuccess = createAction(
    '[Author] Load Posts Success',
    props<{
        authorName: string,
        posts: Post[],
    }>(),
)

export const loadAuthorFailure = createAction(
    '[Author] Load Posts Success',
    props<{ error: string }>(),
)

export const setViewedAuthor = createAction(
    '[Author] Set Viewed Author',
    props<{ authorName: string }>(),
)

export const clearViewedAuthor = createAction(
    '[Author] Clear Viewed Author',
);

export const createPost = createAction(
    '[Posts] Create Post',
    props<{ post: Post }>(),
);

export const createPostSuccess = createAction(
    '[Posts] Create Post Success',
    props<{ post: Post }>(),
);

export const createPostFailure = createAction(
    '[Posts] Create Post Failure',
    props<{ error: string }>(),
)

export const deletePost = createAction(
    '[Posts] Delete Post',
    props<{ postId: number }>(),
);

export const deletePostSuccess = createAction(
    '[Posts] Delete Post Success',
    props<{ postId: number }>(),
)

export const deletePostFailure = createAction(
    '[Posts] Delete Post Failure',
    props<{ error: string }>(),
)
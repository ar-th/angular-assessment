import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { PostsService } from '../services/posts.service';
import * as PostsActions from "./posts-actions";

@Injectable()
export class PostsEffects {
    constructor(
        private actions$: Actions,
        private postsService: PostsService,
    ) { }

    loadPosts$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(PostsActions.loadAll),
            mergeMap(() => this.postsService.getAll()
                .pipe(
                    map(posts => PostsActions.loadAllSuccess({ posts })),
                    catchError(error => of(PostsActions.loadAllFailure({ error }))),
                )
            ),
        );
    });

    loadById$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(PostsActions.loadById),
            mergeMap(action => this.postsService.getById(action.postId)
                .pipe(
                    map(post => PostsActions.loadByIdSuccess({ post })),
                    catchError(error => of(PostsActions.loadByIdFailure({ error })).pipe(
                        tap(error => console.log(error))
                    )),
                )
            ),
        );
    });

    loadAuthor$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(PostsActions.loadAuthor),
            mergeMap(action => this.postsService.getByAuthor(action.authorName)
                .pipe(
                    map(posts => PostsActions.loadAuthorSuccess({ authorName: action.authorName, posts })),
                    catchError(error => of(PostsActions.loadAuthorFailure({ error }))),
                )
            ),
        );
    });

    createPost$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(PostsActions.createPost),
            mergeMap(action => this.postsService.createPost(action.post)
                .pipe(
                    map(() => PostsActions.createPostSuccess({ post: action.post })),
                    catchError(error => of(PostsActions.createPostFailure({ error }))),
                )
            ),
        )
    });

    deletePost$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(PostsActions.deletePost),
            mergeMap(action => this.postsService.deletePost(action.postId)
                .pipe(
                    map(() => PostsActions.deletePostSuccess({ postId: action.postId })),
                    catchError(error => of(PostsActions.deletePostFailure({ error }))),
                )
            ),
        )
    });
}
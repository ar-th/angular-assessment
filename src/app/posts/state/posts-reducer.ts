import { createReducer, on } from "@ngrx/store";
import { Post } from "src/app/shared/types/post.type";
import * as PostsActions from "./posts-actions";
import { PostsState } from "./posts-state.type";

const initialState: PostsState = {
    posts: [],
    authors: [],
    viewedPost: null,
    viewedAuthor: null,
    error: null,
};

export const postsReducer = createReducer<PostsState>(
    initialState,
    on(PostsActions.loadAllSuccess, (state, action) => {
        return {
            ...state,
            posts: action.posts,
            error: null,
        };
    }),
    on(PostsActions.loadAllFailure, (state, action) => {
        return {
            ...state,
            posts: [],
            error: action.error,
        };
    }),
    on(PostsActions.loadByIdSuccess, (state, action) => {
        return {
            ...state,
            posts: [
                ...state.posts.filter(post => post.id !== action.post.id),
                action.post,
            ],
            error: null,
        };
    }),
    on(PostsActions.loadByIdFailure, (state, action) => {
        return {
            ...state,
            error: action.error,
        };
    }),
    on(PostsActions.loadAuthorSuccess, (state, action) => {
        return {
            ...state,
            authors: [
                ...state.authors.filter(author => author.name !== action.authorName),
                {
                    name: action.authorName,
                    posts: action.posts,
                },
            ],
            error: null,
        }
    }),
    on(PostsActions.loadAuthorFailure, (state, action) => {
        return {
            ...state,
            error: action.error,
        };
    }),
    on(PostsActions.createPostSuccess, (state, action) => {
        return {
            ...state,
            posts: [...state.posts, action.post],
            error: null,
        };
    }),
    on(PostsActions.createPostFailure, (state, action) => {
        return {
            ...state,
            error: action.error,
        }
    }),
    on(PostsActions.setViewedPost, (state, action) => {
        return {
            ...state,
            viewedPost: action.postId,
        }
    }),
    on(PostsActions.clearViewedPost, state => {
        return {
            ...state,
            viewedPost: null,
        }
    }),
    on(PostsActions.setViewedAuthor, (state, action) => {
        return {
            ...state,
            viewedAuthor: action.authorName,
        };
    }),
    on(PostsActions.clearViewedAuthor, state => {
        return {
            ...state,
            viewedAuthor: null,
        };
    }),
    on(PostsActions.deletePostSuccess, (state, action) => {
        return {
            ...state,
            posts: state.posts.filter(post => post.id !== action.postId),
            error: null,
        };
    }),
    on(PostsActions.deletePostFailure, (state, action) => {
        return {
            ...state,
            error: action.error,
        };
    }),
);
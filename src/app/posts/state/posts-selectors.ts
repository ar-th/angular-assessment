import { createFeatureSelector, createSelector } from "@ngrx/store";
import { PostsState } from "./posts-state.type";

const getPostsFeatureState = createFeatureSelector<PostsState>('posts');

export const getStateError = createSelector(
    getPostsFeatureState,
    state => state.error,
)

export const getAllPosts = createSelector(
    getPostsFeatureState,
    state => state.posts,
);

export const getViewedPost = createSelector(
    getPostsFeatureState,
    state => state.posts.find(post => post.id === state.viewedPost),
);

export const getViewedAuthorName = createSelector(
    getPostsFeatureState,
    state => state.viewedAuthor,
);

export const getViewedAuthorPosts = createSelector(
    getPostsFeatureState,
    state => state.authors.find(author => author.name === state.viewedAuthor),
);


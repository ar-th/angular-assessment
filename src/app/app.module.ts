import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { StartPageModule } from './start-page/start-page.module';
import { CardModule } from 'primeng/card';
import { StoreModule } from '@ngrx/store';
import { PostsModule } from './posts/posts.module';
import { EffectsModule } from '@ngrx/effects';
import { RouterHistoryModule } from './router-history/router-history.module';
import { ButtonModule } from 'primeng/button';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),

        CardModule,
        ButtonModule,

        StartPageModule,
        PostsModule,
        RouterHistoryModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, filter } from 'rxjs';
import { pathNames } from '../shared/router-paths';

export type HistoryEntry = {
    url: string;
    displayName: string;
};

@Injectable()
export class RouterHistoryService {
    public history$: BehaviorSubject<string[]> = new BehaviorSubject([] as string[]);

    public historyEntries$: BehaviorSubject<HistoryEntry[]> = new BehaviorSubject<HistoryEntry[]>([]);

    private previousUrl?: string;

    constructor(
        private router: Router,
    ) {
        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd))
            .subscribe(({ urlAfterRedirects }: Partial<NavigationEnd>) => {
                const url: string = decodeURIComponent(urlAfterRedirects!);

                const displayName: string = pathNames.get(url.split('/')[1]) || url;

                console.log(
                    pathNames.get(url.split('/')[1]),
                    url.split('/'),
                )

                this.history$.next([
                    url,
                    ...this.history$.value
                ].splice(0, 16));

                this.historyEntries$.next([
                    {
                        url,
                        displayName,
                    },
                    ...this.historyEntries$.value
                ].splice(0, 16));

                this.previousUrl = this.history$.value[1];
            });
    }

    public back(): void {
        if (this.previousUrl) {
            this.router.navigate([this.previousUrl])
        }
    }
}
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { HistoryEntry, RouterHistoryService } from './router-history.service';

@Component({
    selector: 'app-router-history',
    templateUrl: './router-history.component.html',
    styleUrls: ['./router-history.component.scss']
})
export class RouterHistoryComponent {
    @Input()
    public isOpen: boolean = false;

    public history$: Observable<HistoryEntry[]>;

    constructor(
        private historyService: RouterHistoryService,
    ) {
        this.history$ = historyService.historyEntries$;
    }

    public open(): void {
        this.isOpen = true;
    }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SidebarModule } from 'primeng/sidebar';

import { RouterHistoryComponent } from './router-history.component';
import { RouterHistoryService } from './router-history.service';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        RouterModule,

        SidebarModule,
    ],
    exports: [RouterHistoryComponent],
    declarations: [RouterHistoryComponent],
    providers: [RouterHistoryService],
})
export class RouterHistoryModule { }

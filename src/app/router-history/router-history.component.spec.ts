import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterHistoryComponent } from './router-history.component';

describe('RouterHistoryComponent', () => {
  let component: RouterHistoryComponent;
  let fixture: ComponentFixture<RouterHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouterHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

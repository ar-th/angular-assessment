import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPostComponent } from './posts/add-post/add-post.component';
import { AuthorComponent } from './posts/author-posts/author-posts.component';
import { PostDetailsComponent } from './posts/post-details/post-details.component';
import { PostsTableComponent } from './posts/posts-table/posts-table.component';
import { routerPaths } from './shared/router-paths';
import { StartPageComponent } from './start-page/start-page.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: routerPaths.home },
    { path: routerPaths.home, component: StartPageComponent },
    { path: routerPaths.postTable, component: PostsTableComponent },
    { path: routerPaths.addPost, component: AddPostComponent },
    { path: routerPaths.postDetails, children: [
        { path: ":id", component: PostDetailsComponent },
    ]},
    { path: routerPaths.authorPosts, children: [
        { path: ":name", component: AuthorComponent },
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routerPaths = {
    home: 'home',
    postTable: 'table',
    postDetails: 'details',
    authorPosts: 'author',
    addPost: 'add',
};

export const pathNames: Map<string, string> = new Map([
    [routerPaths.home, 'Home Page'],
    [routerPaths.postDetails, 'Post Details'],
    [routerPaths.addPost, 'Create Post'],
    [routerPaths.postTable, 'All Posts'],
    [routerPaths.authorPosts, "Author's Posts"],
]);

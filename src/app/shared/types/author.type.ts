import { Post } from "./post.type"

export type Author = {
    name: string,
    posts: Post[],
}
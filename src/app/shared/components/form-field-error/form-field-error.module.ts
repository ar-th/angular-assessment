import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormFieldErrorComponent } from './form-field-error.component';


@NgModule({
    imports: [
        CommonModule,
    ],
    exports: [FormFieldErrorComponent],
    declarations: [FormFieldErrorComponent],
    providers: [],
})
export class FormFieldErrorModule { }

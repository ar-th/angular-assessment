import { Component } from '@angular/core';
import { MenuItem, PrimeIcons } from 'primeng/api';

@Component({
    selector: 'app-start-page',
    templateUrl: './start-page.component.html',
    styleUrls: ['./start-page.component.scss']
})
export class StartPageComponent {
    public menuItems: MenuItem[] = [
        {
            label: "Home",
            icon: PrimeIcons.HOME,
            routerLink: ['/home'],
        },
        {
            label: "Table",
            icon: PrimeIcons.TABLE,
            routerLink: ['/table'],
        },
        {
            label: "Form",
            icon: PrimeIcons.PLUS_CIRCLE,
            routerLink: ['/add'],
        },
    ];

}

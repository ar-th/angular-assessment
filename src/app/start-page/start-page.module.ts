import { NgModule } from '@angular/core';
import { TabMenuModule } from 'primeng/tabmenu';
import { StartPageComponent } from './start-page.component';

@NgModule({
    imports: [TabMenuModule],
    exports: [StartPageComponent],
    declarations: [StartPageComponent],
    providers: [],
})
export class StartPageModule { }
